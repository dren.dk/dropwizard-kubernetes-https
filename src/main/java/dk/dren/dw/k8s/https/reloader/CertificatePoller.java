package dk.dren.dw.k8s.https.reloader;

import io.dropwizard.jetty.SslReload;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.Base64;

/**
 * This is what does the hashing of certificates to detect changes and possibly calls the SslReload to do the dirty deed.
 */
@RequiredArgsConstructor
@Slf4j
public class CertificatePoller {
    private final CertificateProvider certificateProvider;
    private final SslReload sslReload;
    private String oldHash;

    public void poll() throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("sha1");

        certificateProvider.getCertificateFilesOrURLs().stream().sorted().forEach(s->{
            try {
                digestUrl(digest, new URL(s));
            } catch (MalformedURLException e) { // Hooray for exception based control flow!
                digestFile(digest, new File(s));
            }
        });

        final String newHash = Base64.getEncoder().encodeToString(digest.digest());
        if (newHash.equals(oldHash)) {
            log.debug("No changes found in the files: {} current hash: {}", this, oldHash);
            return;
        }
        if (oldHash == null) {
            oldHash = newHash;
            log.info("Watching for changes to: {} with the current hash: {}", this, oldHash);
        } else {
            log.info("Detected changes in {} {} != {}", this, oldHash, newHash);

            // Dry-run first, so we have a chance that it will work
            sslReload.reloadDryRun();
            // Now we know that configuration is valid, reload for real
            sslReload.reload();
            log.info("Reloaded certificate contained in {}", this);
            oldHash = newHash;
        }
    }

    private void digestUrl(MessageDigest digest, URL url) {
        try {
            final URLConnection urlConnection = url.openConnection();
            urlConnection.connect();

            try (final InputStream is = urlConnection.getInputStream()) {
                digestStream(digest, is);
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed while reading " + url, e);
        }
    }

    private void digestFile(MessageDigest digest, File file) {
        try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
            digestStream(digest, is);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read "+file, e);
        }
    }

    private void digestStream(MessageDigest digest, InputStream is) throws IOException {
        byte[] chunk = new byte[16*1024];
        int bytes;
        while ((bytes=is.read(chunk)) > 0) {
            digest.update(chunk, 0, bytes);
        }
    }

    @Override
    public String toString() {
        return String.join(", ", certificateProvider.getCertificateFilesOrURLs());
    }
}
