package dk.dren.dw.k8s.https;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.annotation.JsonTypeName;
import dk.dren.dw.k8s.https.pemdir.PemDirKeyStore;
import dk.dren.dw.k8s.https.reloader.AutomaticReloader;
import dk.dren.dw.k8s.https.reloader.CertificateProvider;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.jetty.SslReload;
import io.dropwizard.metrics.jetty10.InstrumentedConnectionFactory;
import io.dropwizard.validation.ValidationMethod;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.io.ByteBufferPool;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.util.thread.ScheduledExecutorScheduler;
import org.eclipse.jetty.util.thread.Scheduler;
import org.eclipse.jetty.util.thread.ThreadPool;

import javax.net.ssl.SSLEngine;
import javax.validation.constraints.NotEmpty;
import java.io.File;
import java.net.URI;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * This class is mostly copy+wasted from HttpsConnectorFactory with all the keystore bits changed out.
 *
 * The trust store bits aren't touched, but it's entirely possible to have lists of CA certs in pem files, so
 * it would be reasonable to support that too, even though that's not something commonly found in k8s.
 */
@Slf4j
@JsonTypeName("k8s-https")
@Data
@EqualsAndHashCode(callSuper = false)
public class KubernetesHttpsConnectorFactory extends HttpConnectorFactory implements CertificateProvider {
    private static final AtomicBoolean LOGGED = new AtomicBoolean(false);

    @Nullable
    private String secretPath;

    @Nullable
    private String trustStorePath;

    @Nullable
    private String trustStorePassword;

    @NotEmpty
    private String trustStoreType = "JKS";

    @Nullable
    private String trustStoreProvider;

    @Nullable
    private String keyManagerPassword;

    @Nullable
    private Boolean needClientAuth;

    @Nullable
    private Boolean wantClientAuth;

    @Nullable
    private File crlPath;

    @Nullable
    private Boolean enableCRLDP;

    @Nullable
    private Boolean enableOCSP;

    @Nullable
    private Integer maxCertPathLength;

    @Nullable
    private URI ocspResponderUrl;

    @Nullable
    private String jceProvider;
    private boolean validateCerts = false;
    private boolean validatePeers = false;

    @Nullable
    private List<String> supportedProtocols;

    @Nullable
    private List<String> excludedProtocols = Arrays.asList("SSL.*", "TLSv1", "TLSv1\\.1");

    @Nullable
    private List<String> supportedCipherSuites;

    @Nullable
    private List<String> excludedCipherSuites;

    private boolean allowRenegotiation = true;

    @Nullable
    private String endpointIdentificationAlgorithm;

    private static File[] tlsFiles(File dir) {
        return new File[] {
                new File(dir, "tls.key"),
                new File(dir, "tls.crt")
        };
    }

    @ValidationMethod(message = "secretPath should be set and point to a directory containing tls.key and tls.crt")
    public boolean isValidSecretPath() {
        if (secretPath == null) {
            return false; // No, it really needs to be set.

        } else {
            final File dir = new File(secretPath);
            if (dir.isDirectory()) {
                for (File file : tlsFiles(dir)) {
                    if (!file.isFile()) {
                        log.warn("Not a file: {}", file);
                        return false;
                    }
                    if (!file.canRead()) {
                        log.warn("Not a readable file: {}", file);
                        return false;
                    }
                }
                return true;
            } else {
                log.warn("Not a directory: {}", dir);
                return false;
            }
        }

    }

    @Override
    public Connector build(Server server, MetricRegistry metrics, String name, @Nullable ThreadPool threadPool) {
        final HttpConfiguration httpConfig = buildHttpConfiguration();

        final HttpConnectionFactory httpConnectionFactory = buildHttpConnectionFactory(httpConfig);

        final SslContextFactory.Server sslContextFactory = new SslContextFactory.Server();
        configureSslContextFactory(sslContextFactory);
        sslContextFactory.addEventListener(logSslInfoOnStart(sslContextFactory));

        server.addBean(sslContextFactory);
        final SslReload reloader = new SslReload(sslContextFactory, this::configureSslContextFactory);
        server.addBean(reloader);
        AutomaticReloader.register(this, reloader);

        final SslConnectionFactory sslConnectionFactory =
                new SslConnectionFactory(sslContextFactory, HttpVersion.HTTP_1_1.toString());

        final Scheduler scheduler = new ScheduledExecutorScheduler();

        final ByteBufferPool bufferPool = buildBufferPool();

        return buildConnector(server, scheduler, bufferPool, name, threadPool,
                new InstrumentedConnectionFactory(
                        sslConnectionFactory,
                        metrics.timer(httpConnections())),
                httpConnectionFactory);
    }

    @Override
    protected HttpConfiguration buildHttpConfiguration() {
        final HttpConfiguration config = super.buildHttpConfiguration();
        config.setSecureScheme("https");
        config.setSecurePort(getPort());
        config.addCustomizer(new SecureRequestCustomizer());
        return config;
    }

    /** Register a listener that waits until the ssl context factory has started. Once it has
     *  started we can grab the fully initialized context so we can log the parameters.
     */
    protected AbstractLifeCycle.AbstractLifeCycleListener logSslInfoOnStart(final SslContextFactory sslContextFactory) {
        return new AbstractLifeCycle.AbstractLifeCycleListener() {
            @Override
            public void lifeCycleStarted(LifeCycle event) {
                logSupportedParameters(sslContextFactory);
            }
        };
    }

    /**
     * Given a list of protocols available to the JVM that we can serve up to the client, partition
     * this list into two groups: a group of protocols we can serve and a group where we can't. This
     * list takes into account protocols that may have been disabled at the JVM level, and also
     * protocols that the user explicitly wants to include / exclude. The exclude list (blacklist)
     * is stronger than include list (whitelist), so a protocol that is in both lists will be
     * excluded. Other than the initial list of available protocols, the other lists are patterns,
     * such that one can exclude all SSL protocols with a single exclude entry of "SSL.*". This
     * function will handle both cipher suites and protocols, but for the sake of conciseness, this
     * documentation only talks about protocols. This implementation is a slimmed down version from
     * jetty:
     * https://github.com/eclipse/jetty.project/blob/93a8afcc6bd1a6e0af7bd9f967c97ae1bc3eb718/jetty-util/src/main/java/org/eclipse/jetty/util/ssl/SslSelectionDump.java
     *
     * @param supportedByJVM protocols available to the JVM.
     * @param enabledByJVM protocols enabled by lib/security/java.security.
     * @param excludedByConfig protocols the user doesn't want to expose.
     * @param includedByConfig the only protocols the user wants to expose.
     * @return two entry map of protocols that are enabled (true) and those that have been disabled (false).
     */
    static Map<Boolean, List<String>> partitionSupport(
            String[] supportedByJVM,
            String[] enabledByJVM,
            String[] excludedByConfig,
            String[] includedByConfig
    ) {
        final List<Pattern> enabled = Arrays.stream(enabledByJVM).map(Pattern::compile).collect(Collectors.toList());
        final List<Pattern> disabled = Arrays.stream(excludedByConfig).map(Pattern::compile).collect(Collectors.toList());
        final List<Pattern> included = Arrays.stream(includedByConfig).map(Pattern::compile).collect(Collectors.toList());

        return Arrays.stream(supportedByJVM)
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.partitioningBy(x ->
                        disabled.stream().noneMatch(pat -> pat.matcher(x).matches()) &&
                                enabled.stream().anyMatch(pat -> pat.matcher(x).matches()) &&
                                (included.isEmpty() || included.stream().anyMatch(pat -> pat.matcher(x).matches()))
                ));

    }

    private void logSupportedParameters(SslContextFactory contextFactory) {
        if (LOGGED.compareAndSet(false, true)) {
            // When Jetty logs out which protocols are enabled / disabled they include tracing
            // information to detect if the protocol was disabled at the
            // JRE/lib/security/java.security level. Since we don't log this information we take the
            // SSLEngine from our context instead of a pristine version.
            //
            // For more info from Jetty:
            // https://github.com/eclipse/jetty.project/blob/93a8afcc6bd1a6e0af7bd9f967c97ae1bc3eb718/jetty-util/src/main/java/org/eclipse/jetty/util/ssl/SslContextFactory.java#L356-L360
            final SSLEngine engine = contextFactory.getSslContext().createSSLEngine();

            final Map<Boolean, List<String>> protocols = partitionSupport(
                    engine.getSupportedProtocols(),
                    engine.getEnabledProtocols(),
                    contextFactory.getExcludeProtocols(),
                    contextFactory.getIncludeProtocols()
            );

            final Map<Boolean, List<String>> ciphers = partitionSupport(
                    engine.getSupportedCipherSuites(),
                    engine.getEnabledCipherSuites(),
                    contextFactory.getExcludeCipherSuites(),
                    contextFactory.getIncludeCipherSuites()
            );

            log.info("Enabled protocols: "+ protocols.get(true));
            log.info("Disabled protocols: "+ protocols.get(false));
            log.info("Enabled cipher suites: "+ ciphers.get(true));
            log.info("Disabled cipher suites: "+ciphers.get(false));
        }
    }

    @Override
    public Collection<String> getCertificateFilesOrURLs() {
        assert secretPath != null; // Because it gets validated long before this method can be called.
        final List<String> allCertificateRelatedFiles = Arrays.stream(tlsFiles(new File(secretPath))).map(File::getAbsolutePath).collect(Collectors.toList());
        if (trustStorePath != null) {
            allCertificateRelatedFiles.add(trustStorePath);
        }
        return allCertificateRelatedFiles;
    }


    protected void configureSslContextFactory(SslContextFactory factory) {

        try {
            factory.setKeyStore(new PemDirKeyStore(tlsFiles(new File(secretPath))));
        } catch (Exception e) {
            throw new IllegalStateException("Failed to load certificate from "+secretPath, e);
        }

        if (trustStorePath != null) {
            factory.setTrustStorePath(trustStorePath);
        }
        if (trustStorePassword != null) {
            factory.setTrustStorePassword(trustStorePassword);
        }
        factory.setTrustStoreType(trustStoreType);

        if (trustStoreProvider != null) {
            factory.setTrustStoreProvider(trustStoreProvider);
        }

        if (keyManagerPassword != null) {
            factory.setKeyManagerPassword(keyManagerPassword);
        }

        if (needClientAuth != null && factory instanceof SslContextFactory.Server) {
            ((SslContextFactory.Server) factory).setNeedClientAuth(needClientAuth);
        }

        if (wantClientAuth != null && factory instanceof SslContextFactory.Server) {
            ((SslContextFactory.Server) factory).setWantClientAuth(wantClientAuth);
        }

        factory.setCertAlias("pem");

        if (crlPath != null) {
            factory.setCrlPath(crlPath.getAbsolutePath());
        }

        if (enableCRLDP != null) {
            factory.setEnableCRLDP(enableCRLDP);
        }

        if (enableOCSP != null) {
            factory.setEnableOCSP(enableOCSP);
        }

        if (maxCertPathLength != null) {
            factory.setMaxCertPathLength(maxCertPathLength);
        }

        if (ocspResponderUrl != null) {
            factory.setOcspResponderURL(ocspResponderUrl.toASCIIString());
        }

        if (jceProvider != null) {
            factory.setProvider(jceProvider);
        }

        factory.setRenegotiationAllowed(allowRenegotiation);
        factory.setEndpointIdentificationAlgorithm(endpointIdentificationAlgorithm);

        factory.setValidateCerts(validateCerts);
        factory.setValidatePeerCerts(validatePeers);

        if (supportedProtocols != null) {
            factory.setIncludeProtocols(supportedProtocols.toArray(new String[0]));
        }

        if (excludedProtocols != null) {
            factory.setExcludeProtocols(excludedProtocols.toArray(new String[0]));
        }

        if (supportedCipherSuites != null) {
            factory.setIncludeCipherSuites(supportedCipherSuites.toArray(new String[0]));
        }

        if (excludedCipherSuites != null) {
            factory.setExcludeCipherSuites(excludedCipherSuites.toArray(new String[0]));
        }
    }
}
