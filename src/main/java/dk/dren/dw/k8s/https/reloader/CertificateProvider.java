package dk.dren.dw.k8s.https.reloader;

import java.util.Collection;

/**
 * This interface should be implemented by connector factories or others that want to be able to automatically reload
 * certificates.
 *
 * Files and URLS are hashed to figure out if anything has changed.
 */
public interface CertificateProvider {
    Collection<String> getCertificateFilesOrURLs();
}
