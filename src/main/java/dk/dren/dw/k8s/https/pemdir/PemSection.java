package dk.dren.dw.k8s.https.pemdir;

import java.io.ByteArrayInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

/**
 * A section of a PEM file, with support for parsing X509 Certificates, as well as private keys in PKCS#8 and PKCS#1
 */
public class PemSection {
    private final int line;
    private final String label;
    private final String body;

    public PemSection(int line, String label, String body) {
        this.line = line;
        this.label = label;
        this.body = body;
    }

    public String getLabel() {
        return label;
    }

    public String getBody() {
        return body;
    }

    public int getLine() {
        return line;
    }

    public byte[] getBodyBytes() {
        return Base64.getDecoder().decode(body);
    }

    public boolean isCertificate() {
        return label.equals("CERTIFICATE");
    }

    public X509Certificate getCertificate() throws CertificateException {
        return (X509Certificate) CertificateFactory.getInstance("X.509")
            .generateCertificate(new ByteArrayInputStream(getBodyBytes()));
    }

    public boolean isPKCS1RSAPrivateKey() {
        return label.equals("RSA PRIVATE KEY");
    }

    public boolean isPKCS8PrivateKey() {
        return label.equals("PRIVATE KEY");
    }

    public RSAPrivateKey getPKCS1RSAPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        return PemPrivateKeyDecoder.readPkcs1PrivateKey(getBodyBytes());
    }

    public RSAPrivateKey getPkcs8PrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        return PemPrivateKeyDecoder.readPkcs8PrivateKey(getBodyBytes());
    }

    public boolean isRSAPrivateKey() {
        return isPKCS1RSAPrivateKey() || isPKCS8PrivateKey();
    }

    public RSAPrivateKey getRSAPrivateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (isPKCS1RSAPrivateKey()) {
            return getPKCS1RSAPrivateKey();
        } else if (isPKCS8PrivateKey()) {
            return getPkcs8PrivateKey();
        } else {
            throw new IllegalArgumentException("The section at line "+line+" with the label "+ label +" is not a private key");
        }
    }
}
