# Automatic TLS certificate reloads

This package contains everything needed to enable reloading of certificates whenever they change,
two things are needed to make this work:

* ConnectorFactory must implement CertificateProvider
* ConnectorFactory.build() must call AutomaticReloader.register()

The AutomaticReloader will then poll the files for changed content and call the SslReloader
that was registered.

If AutomaticReloader.register() is called, then it will start a thread that will spend an hour
sleeping between polling the certificate files for changes.
