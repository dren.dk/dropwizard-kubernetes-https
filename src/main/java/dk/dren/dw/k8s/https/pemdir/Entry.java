package dk.dren.dw.k8s.https.pemdir;

import java.security.Key;
import java.security.cert.Certificate;
import org.checkerframework.checker.nullness.qual.Nullable;
/**
 * An entry in the keystore
 *
 * cut-down version of the more complete pem-keystore here: https://github.com/ctron/pem-keystore
 */
public final class Entry {

    @Nullable
    private final Key key;
    @Nullable
    private final Certificate[] certificateChain;

    public Entry(@Nullable final Key key, @Nullable final Certificate[] certificateChain) {
        this.key = key;

        if (certificateChain != null && certificateChain.length > 0) {
            this.certificateChain = certificateChain;
        } else {
            this.certificateChain = null;
        }
    }

    public @Nullable Key getKey() {
        return this.key;
    }

    public boolean isKey() {
        return this.key != null;
    }

    public @Nullable Certificate[] getCertificateChain() {
        if (this.certificateChain == null) {
            return null;
        }
        return this.certificateChain.clone();
    }

    public @Nullable Certificate getCertificate() {
        if (this.certificateChain == null) {
            return null;
        }
        return this.certificateChain[0];
    }

    public boolean isCertificate() {
        return this.certificateChain != null;
    }

    public Entry merge(final Entry other) {

        if (other == null) {
            return this;
        }

        Key key = other.key;
        Certificate[] certificateChain = other.certificateChain;

        if (key == null) {
            key = this.key;
        }
        if (certificateChain == null) {
            certificateChain = this.certificateChain;
        }

        return new Entry(key, certificateChain);
    }
}
