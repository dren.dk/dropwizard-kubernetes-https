package dk.dren.dw.k8s.https.pemdir;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.KeyStoreSpi;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;
import org.checkerframework.checker.nullness.qual.Nullable;
/**
 * Annoying glue class required by the JDK Keystore API
 * cut-down version of the more complete pem-keystore here: https://github.com/ctron/pem-keystore
 */
public class PemDirKeyStoreSpi extends KeyStoreSpi {
    private final File[] tlsFiles;
    @Nullable
    private Map<String, Entry> entryByAlias;

    public PemDirKeyStoreSpi(File[] tlsFiles) throws CertificateException {
        this.tlsFiles = tlsFiles;
    }

    @Override
    public void engineLoad(final InputStream stream, final char[] password)
            throws IOException, NoSuchAlgorithmException, CertificateException {
        // The parameters are ignored entirely, because both are useless for our purposes and the entire reason the pemdir keystore exists
        entryByAlias = PemLoader.loadFromFiles(tlsFiles);
    }

    protected Optional<Entry> getEntry(final String alias) {
        if (entryByAlias == null) {
            throw new IllegalStateException("Not yet loaded");
        }

        final Entry entry = entryByAlias.get(alias);
        if (entry == null) {
            return Optional.empty();
        } else {
            return Optional.of(entry);
        }
    }

    @Override
    public @Nullable Key engineGetKey(final String alias, final char[] password) {

        return getEntry(alias)
                .map(Entry::getKey)
                .orElse(null);

    }

    @Override
    public boolean engineIsKeyEntry(final String alias) {

        return getEntry(alias)
                .map(Entry::isKey)
                .orElse(false);

    }

    @Override
    public @Nullable Certificate[] engineGetCertificateChain(final String alias) {

        return getEntry(alias)
                .map(Entry::getCertificateChain)
                .orElse(null);

    }

    @Override
    public @Nullable Certificate engineGetCertificate(final String alias) {

        return getEntry(alias)
                .map(Entry::getCertificate)
                .orElse(null);

    }

    @Override
    public @Nullable Date engineGetCreationDate(final String alias) {

        return getEntry(alias)
                .map(Entry::getCertificate)
                .map(cert -> cert instanceof X509Certificate ? (X509Certificate) cert : null)
                .map(X509Certificate::getNotBefore)
                .orElse(null);

    }

    @Override
    public Enumeration<String> engineAliases() {
        if (entryByAlias == null) {
            throw new IllegalStateException("Not initialized");
        }
        final Iterator<String> keys = this.entryByAlias.keySet().iterator();

        return new Enumeration<String>() {

            @Override
            public String nextElement() {
                return keys.next();
            }

            @Override
            public boolean hasMoreElements() {
                return keys.hasNext();
            }
        };
    }

    @Override
    public boolean engineContainsAlias(final String alias) {
        if (entryByAlias == null) {
            throw new IllegalStateException("Not initialized");
        }
        return this.entryByAlias.containsKey(alias);
    }

    @Override
    public int engineSize() {
        if (entryByAlias == null) {
            throw new IllegalStateException("Not initialized");
        }
        return this.entryByAlias.size();
    }

    @Override
    public boolean engineIsCertificateEntry(final String alias) {

        return getEntry(alias)
                .map(Entry::isCertificate)
                .orElse(false);

    }

    @Override
    public @Nullable String engineGetCertificateAlias(final Certificate cert) {
        if (entryByAlias == null) {
            throw new IllegalStateException("Not initialized");
        }

        if (!(cert instanceof Certificate)) {
            return null;
        }

        for (final Map.Entry<String, Entry> entry : this.entryByAlias.entrySet()) {

            if (cert == entry.getValue().getCertificate()) {
                return entry.getKey();
            }

        }

        return null;
    }

    protected Map<String, Entry> initializeEmpty() {
        return Collections.emptyMap();
    }

    @Override
    public void engineSetKeyEntry(final String alias, final Key key, final char[] password, final Certificate[] chain)
            throws KeyStoreException {
        throw new KeyStoreException("Unsupported operation");
    }

    @Override
    public void engineSetKeyEntry(final String alias, final byte[] key, final Certificate[] chain)
            throws KeyStoreException {
        throw new KeyStoreException("Unsupported operation");
    }

    @Override
    public void engineSetCertificateEntry(final String alias, final Certificate cert) throws KeyStoreException {
        throw new KeyStoreException("Unsupported operation");
    }

    @Override
    public void engineDeleteEntry(final String alias) throws KeyStoreException {
        throw new KeyStoreException("Unsupported operation");
    }

    @Override
    public void engineStore(final OutputStream stream, final char[] password)
            throws IOException, NoSuchAlgorithmException, CertificateException {

        throw new IOException("Unsupported operation");
    }
}
