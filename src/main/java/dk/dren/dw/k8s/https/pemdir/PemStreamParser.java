package dk.dren.dw.k8s.https.pemdir;

import org.checkerframework.checker.nullness.qual.Nullable;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser for PEM streams, which can contain any number of sections each with a name and a base64 encoded body
 */
public class PemStreamParser {

    public static final Pattern HEADER_FOOTER = Pattern.compile("-----* *(BEGIN|END) ([^-]+)-----* *");
    private final List<PemSection> sections = new ArrayList<>();
    private int currentLine = 0;
    private int sectionStartLine = 0;
    private @Nullable String currentLabel;
    private @Nullable StringBuffer currentBody;

    public PemStreamParser(InputStream is) {
        new BufferedReader(new InputStreamReader(is)).lines()
            .forEach(this::addLine);

        if (currentLabel != null) {
            throw new IllegalArgumentException("Found EOF at line " + currentLine + " but expected to find the end of " + currentLabel);
        }
    }

    private void addLine(String line) {
        currentLine++;

        if (line.matches("\\s*")) {
            return; // Skip empty lines, they do not matter at all
        }

        final Matcher headerOrFooter = HEADER_FOOTER.matcher(line);
        if (headerOrFooter.matches()) {
            final String label = headerOrFooter.group(2).trim();
            if (headerOrFooter.group(1).equals("BEGIN")) {
                if (currentLabel != null) {
                    throw new IllegalArgumentException("Found "+line+" at line "+currentLine+" but expected to find either base64 content or the end of "+ currentLabel +" in stead");
                }
                sectionStartLine = currentLine;
                currentLabel = label;
                currentBody = new StringBuffer();
            } else {
                // END
                if (currentLabel == null || currentBody == null) {
                    throw new IllegalArgumentException("Found "+line+" at line "+currentLine+" but expected to find the beginning of a section in stead");
                }
                if (label.equals(currentLabel)) {
                    sections.add(new PemSection(sectionStartLine, currentLabel, currentBody.toString()));
                    currentBody = null;
                    currentLabel = null;
                } else {
                    throw new IllegalArgumentException("Found "+line+" at line "+currentLine+" but expected to find the end of "+ currentLabel +" in stead");
                }
            }

        } else {
            if (currentLabel == null || currentBody == null) {
                throw new IllegalArgumentException("Found "+line+" at line "+currentLine+" but expected to find a BEGIN header");
            }

            // Accumulate all the non-whitespace found
            currentBody.append(line.replaceAll("\\s+", ""));
        }
    }

    public List<PemSection> getSections() {
        return sections;
    }
}
