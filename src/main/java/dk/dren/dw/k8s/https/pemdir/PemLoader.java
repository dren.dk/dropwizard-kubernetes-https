package dk.dren.dw.k8s.https.pemdir;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.Key;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This does the work of merging the various tls files into a single entry.
 * Inspired by the more complete pem-keystore here: https://github.com/ctron/pem-keystore
 */
public class PemLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PemLoader.class);

    public static Map<String, Entry> loadFromFiles(File[] tlsFiles) {
        final Map<String, Entry> result = new TreeMap<>();

        for (File tlsFile : tlsFiles) {
            try {
                readSections(loadPemFile(tlsFile), result);
            } catch (Exception e) {
                throw new RuntimeException("Failed while loading "+tlsFile, e);
            }
        }

        return result;
    }

    private static void readSections(PemStreamParser pem, Map<String, Entry> result) {

        final List<Certificate> chain = new ArrayList<>();
        Key key = null;
        for (PemSection section : pem.getSections()) {
            try {
                if (section.isCertificate()) {
                    chain.add(section.getCertificate());
                } else if (section.isRSAPrivateKey()) {
                    key = section.getRSAPrivateKey();
                } else {
                    LOGGER.warn("Ignoring {} at line {} ", section.getLabel(), section.getLine());
                }
            } catch (Exception e) {
                throw new RuntimeException("Failed while reading "+section.getLabel()+" at line "+section.getLine(), e);
            }
        }

        final Certificate[] certificateChain = chain.isEmpty() ? null
                : chain.toArray(new Certificate[0]);

        final Entry e = new Entry(key, certificateChain);

        result.compute("pem", (k, v) -> {
            if (v != null) {
                return v.merge(e);
            } else {
                return e;
            }
        });
    }

    private static PemStreamParser loadPemFile(File tlsFile) throws IOException {
        try (final InputStream is = new BufferedInputStream(new FileInputStream(tlsFile))) {
            return new PemStreamParser(is);
        }
    }
}
