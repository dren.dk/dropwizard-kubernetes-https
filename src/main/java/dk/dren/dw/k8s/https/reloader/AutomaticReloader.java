package dk.dren.dw.k8s.https.reloader;

import io.dropwizard.jetty.SslReload;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * This is a centralized polling thread that polls the certificates that are in use by the CertificateProviders
 * and if changes are seen, then the associated SslReload is called to reload the certificates.
 */
@Slf4j
public class AutomaticReloader {
    private final List<CertificatePoller> pollers = new ArrayList<>();

    @Getter(lazy=true)
    private static final AutomaticReloader instance = AutomaticReloader.create();
    private Thread thread;

    @Getter @Setter
    private long interval = TimeUnit.HOURS.toMillis(1);

    private static AutomaticReloader create() {
        return new AutomaticReloader();
    }

    public static void register(CertificateProvider certificateProvider, SslReload sslReload) {
        getInstance().add(certificateProvider, sslReload);
    }

    private synchronized void add(CertificateProvider certificateProvider, SslReload sslReload) {
        final CertificatePoller poller = new CertificatePoller(certificateProvider, sslReload);
        try {
            poller.poll();
        } catch (Exception e) {
            throw new RuntimeException("Failed to poll the certificates once: "+poller, e);
        }
        pollers.add(poller);
        startPollThread();
    }

    private synchronized void startPollThread() {
        if (thread == null) {
            thread = new Thread(this::loop);
            thread.setDaemon(true);
            thread.setName("Certificate Reloader");
            thread.start();
        }
    }

    private void loop() {
        while (!thread.isInterrupted()) {
            try {
                Thread.sleep(interval);
                poll();
            } catch (Exception e) {
                log.warn("Failed while polling for changed certificates", e);
            }
        }
    }

    private synchronized void poll() {
        for (CertificatePoller poller : pollers) {
            try {
                poller.poll();
            } catch (Exception e) {
                log.warn("Failed while polling {}", poller, e);
            }
        }
    }
}
