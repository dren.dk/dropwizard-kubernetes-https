package dk.dren.dw.k8s.https.pemdir;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

/**
 * A Keystore that's loaded from a bunch of pem files in a directory
 */
public class PemDirKeyStore extends KeyStore {
    public static final String KEYSTORE_TYPE_NAME = "pemdir";
    private final File[] files;

    public PemDirKeyStore(File[] files) throws CertificateException, IOException, NoSuchAlgorithmException {
        super(new PemDirKeyStoreSpi(files), null, KEYSTORE_TYPE_NAME);
        this.files = files;
        load(null, null);
    }

    public File[] getFiles() {
        return files;
    }
}
