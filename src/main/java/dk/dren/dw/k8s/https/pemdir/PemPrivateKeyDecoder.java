package dk.dren.dw.k8s.https.pemdir;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

import static com.google.common.primitives.Bytes.concat;

/**
 * Private key parser mainly for working around the limitations of the java security API
 * which makes straight-forward parsing of PKCS#1 impossible.
 *
 * Shamelessly lifted from: https://github.com/Mastercard/client-encryption-java/blob/main/src/main/java/com/mastercard/developer/utils/EncryptionUtils.java
 */
public class PemPrivateKeyDecoder {

    /**
     * Create a PrivateKey instance from raw PKCS#8 bytes.
     */
    public static RSAPrivateKey readPkcs8PrivateKey(byte[] pkcs8Bytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8Bytes);
        return (RSAPrivateKey)keyFactory.generatePrivate(keySpec);
    }

    /**
     * Create a PrivateKey instance from raw PKCS#1 bytes.
     */
    public static RSAPrivateKey readPkcs1PrivateKey(byte[] pkcs1Bytes) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // We can't use Java internal APIs to parse ASN.1 structures, so we build a PKCS#8 key Java can understand
        int pkcs1Length = pkcs1Bytes.length;
        int totalLength = pkcs1Length + 22;
        byte[] pkcs8Header = new byte[]{
            0x30, (byte) 0x82, (byte) ((totalLength >> 8) & 0xff), (byte) (totalLength & 0xff), // Sequence + total length
            0x2, 0x1, 0x0, // Integer (0)
            0x30, 0xD, 0x6, 0x9, 0x2A, (byte) 0x86, 0x48, (byte) 0x86, (byte) 0xF7, 0xD, 0x1, 0x1, 0x1, 0x5, 0x0, // Sequence: 1.2.840.113549.1.1.1, NULL
            0x4, (byte) 0x82, (byte) ((pkcs1Length >> 8) & 0xff), (byte) (pkcs1Length & 0xff) // Octet string + length
        };
        byte[] pkcs8bytes = concat(pkcs8Header, pkcs1Bytes);
        return readPkcs8PrivateKey(pkcs8bytes);
    }
}
