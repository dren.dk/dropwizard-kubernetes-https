package dk.dren.dw.k8s.https.pemdir;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

public class PemStreamParserTest {
    @Test
    public void testGoodTlsCertificateChain() throws CertificateException {
        final PemStreamParser psp = new PemStreamParser(PemStreamParserTest.class.getResourceAsStream("tls.crt.pem"));
        final List<PemSection> sections = psp.getSections();
        Assertions.assertEquals(3, sections.size());

        Assertions.assertEquals("CERTIFICATE", sections.get(0).getLabel());
        Assertions.assertEquals(1, sections.get(0).getLine());
        Assertions.assertEquals(1344, sections.get(0).getBodyBytes().length);
        Assertions.assertTrue(sections.get(0).isCertificate());
        Assertions.assertEquals("CN=staging.subversion.stibo.dk",
            sections.get(0).getCertificate().getSubjectX500Principal().getName());

        Assertions.assertEquals("CERTIFICATE", sections.get(1).getLabel());
        Assertions.assertEquals(31, sections.get(1).getLine());
        Assertions.assertEquals(1306, sections.get(1).getBodyBytes().length);
        Assertions.assertTrue(sections.get(1).isCertificate());
        Assertions.assertEquals("CN=R3,O=Let's Encrypt,C=US",
            sections.get(1).getCertificate().getSubjectX500Principal().getName());

        Assertions.assertEquals("CERTIFICATE", sections.get(2).getLabel());
        Assertions.assertEquals(61, sections.get(2).getLine());
        Assertions.assertEquals(1380, sections.get(2).getBodyBytes().length);
        Assertions.assertTrue(sections.get(2).isCertificate());
        Assertions.assertEquals("CN=ISRG Root X1,O=Internet Security Research Group,C=US",
            sections.get(2).getCertificate().getSubjectX500Principal().getName());
    }

    @Test
    public void testGoodPkcs1TlsKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        final PemStreamParser psp = new PemStreamParser(PemStreamParserTest.class.getResourceAsStream("tls.key.pkcs1.pem"));
        final List<PemSection> sections = psp.getSections();
        Assertions.assertEquals(1, sections.size());

        final PemSection keySection = sections.get(0);
        Assertions.assertEquals("RSA PRIVATE KEY", keySection.getLabel());
        Assertions.assertEquals(1, keySection.getLine());
        Assertions.assertEquals(1192, keySection.getBodyBytes().length);
        Assertions.assertTrue(keySection.isPKCS1RSAPrivateKey());
        Assertions.assertTrue(keySection.isRSAPrivateKey());
        final RSAPrivateKey rsaPrivateKey = keySection.getRSAPrivateKey();

        Assertions.assertEquals("PKCS#8", rsaPrivateKey.getFormat());
        Assertions.assertEquals("RSA", rsaPrivateKey.getAlgorithm());
    }

    @Test
    public void testGoodTlsKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        final PemStreamParser psp = new PemStreamParser(PemStreamParserTest.class.getResourceAsStream("tls.key.pkcs8.pem"));
        final List<PemSection> sections = psp.getSections();
        Assertions.assertEquals(1, sections.size());

        final PemSection keySection = sections.get(0);
        Assertions.assertEquals("PRIVATE KEY", keySection.getLabel());
        Assertions.assertEquals(1, keySection.getLine());
        Assertions.assertEquals(1218, keySection.getBodyBytes().length);
        Assertions.assertTrue(keySection.isPKCS8PrivateKey());
        Assertions.assertTrue(keySection.isRSAPrivateKey());
        final RSAPrivateKey rsaPrivateKey = keySection.getRSAPrivateKey();

        Assertions.assertEquals("PKCS#8", rsaPrivateKey.getFormat());
        Assertions.assertEquals("RSA", rsaPrivateKey.getAlgorithm());
    }

}
